\begin{thebibliography}{100}
\def\selectlanguageifdefined#1{
\expandafter\ifx\csname date#1\endcsname\relax
\else\selectlanguage{#1}\fi}
\providecommand*{\href}[2]{{\small #2}}
\providecommand*{\url}[1]{{\small #1}}
\providecommand*{\BibUrl}[1]{\url{#1}}
\providecommand{\BibAnnote}[1]{}
\providecommand*{\BibEmph}[1]{#1}
\ProvideTextCommandDefault{\cyrdash}{\iflanguage{russian}{\hbox
  to.8em{--\hss--}}{\textemdash}}
\providecommand*{\BibDash}{\ifdim\lastskip>0pt\unskip\nobreak\hskip.2em plus
  0.1em\fi
\cyrdash\hskip.2em plus 0.1em\ignorespaces}
\renewcommand{\newblock}{\ignorespaces}

\bibitem{PlakidatJ}
\selectlanguageifdefined{english}
\BibEmph{Plakida,~N.~M.} High-Temperature Cuprate Superconductors. Experiment,
  Theory, and Applications [Text]~/ N.~M.~Plakida. \BibDash
\newblock Berlin~: Springer, 2011.

\bibitem{ScalapinoRevModPhys2012}
\selectlanguageifdefined{english}
\BibEmph{Scalapino,~D.~J.} A common thread: The pairing interaction for
  unconventional superconductors [Text]~/ D.~J.~Scalapino~// \BibEmph{Rev. Mod.
  Phys.} \BibDash
\newblock 2012. \BibDash
\newblock Vol.~84. \BibDash
\newblock P.~1383--1417.

\bibitem{AndersonHTSC}
\selectlanguageifdefined{english}
\BibEmph{Anderson,~P.~W.} The Theory of Superconductivity in High-$T_c$
  Cuprates [Text]~/ P.~W.~Anderson. \BibDash
\newblock Princeton~: Princeton University Press, 1997.

\bibitem{NagaosaRevModPhys2006}
\selectlanguageifdefined{english}
\BibEmph{Lee,~P.~A.} Doping a mott insulator: Physics of high-temperature
  superconductivity [Text]~/ Patrick~A.~Lee, Naoto~Nagaosa, Xiao-Gang~Wen~//
  \BibEmph{Rev. Mod. Phys.} \BibDash
\newblock 2006. \BibDash
\newblock Vol.~78. \BibDash
\newblock P.~17--85.

\bibitem{Eremets203}
\selectlanguageifdefined{english}
Conventional superconductivity at 203 kelvin at high pressures in the sulfur
  hydride system [Text]~/ A.~P.~Drozdov, M.~I.~Eremets, I.~A.~Troyan
  [et~al.]~// \BibEmph{Nature}. \BibDash
\newblock 2015. \BibDash
\newblock Vol. 525. \BibDash
\newblock P.~73.

\bibitem{TopologicalRevModPhys}
\selectlanguageifdefined{english}
\BibEmph{Qi,~X.-L.} Topological insulators and superconductors [Text]~/
  Xiao-Liang~Qi, Shou-Cheng~Zhang~// \BibEmph{Rev. Mod. Phys.} \BibDash
\newblock 2011. \BibDash
\newblock Vol.~83. \BibDash
\newblock P.~1057--1110.

\bibitem{TopologicalSchnyder}
\selectlanguageifdefined{english}
\BibEmph{Schnyder,~A.~P.} Topological surface states in nodal superconductors
  [Text]~/ Andreas~P.~Schnyder, Philip M.~R.~Brydon~// \BibEmph{Journal of
  Physics: Condensed Matter}. \BibDash
\newblock 2015. \BibDash
\newblock Vol.~27, no.~24. \BibDash
\newblock P.~243201.

\bibitem{TakasanLaserTop}
\selectlanguageifdefined{english}
Laser-induced topological superconductivity in cuprate thin films [Text]~/
  Kazuaki~Takasan, Akito~Daido, Norio~Kawakami, Youichi~Yanase~//
  \BibEmph{Phys. Rev. B}. \BibDash
\newblock 2017. \BibDash
\newblock Vol.~95. \BibDash
\newblock P.~134508.

\bibitem{PlakidaTc2016}
\selectlanguageifdefined{english}
\BibEmph{Plakida,~N.} Spin fluctuations and high-temperature superconductivity
  in cuprates [Text]~/ N.~Plakida~// \BibEmph{Physica C - Superconductivity and
  its applications}. \BibDash
\newblock 2016. \BibDash
\newblock Vol. 531. \BibDash
\newblock P.~39--59.

\bibitem{DzebSpPol}
\selectlanguageifdefined{russian}
\BibEmph{�������,~�.~�.} ����-���������� ������� ����������� ����������� � ��
  d- �������� ���������� � ��������� ����������������
  [{\cyr\CYRT\cyre\cyrk\cyrs\cyrt}]~/ �.~�.~�������, �.~�.~������������,
  �.~�.~���������~// \BibEmph{������ � ����}. \BibDash
\newblock 2016. \BibDash
\newblock \CYRT. 104. \BibDash
\newblock {\cyr\CYRS.}~745.

\bibitem{MaximovDolgovColoumb}
\selectlanguageifdefined{russian}
\BibEmph{��������,~�.~�.} � ��������� ���������� �������������������
  ����������������� [{\cyr\CYRT\cyre\cyrk\cyrs\cyrt}]~/ �.~�.~��������,
  �.~�.~������~// \BibEmph{���}. \BibDash
\newblock 2007. \BibDash
\newblock \CYRT. 177. \BibDash
\newblock {\cyr\CYRS.}~983.

\bibitem{DamascelliPhase}
\selectlanguageifdefined{english}
\BibEmph{Damascelli,~A.} Angle-resolved photoemission studies of the cuprate
  superconductors [Text]~/ Andrea~Damascelli, Zahid~Hussain, Zhi-Xun~Shen~//
  \BibEmph{Rev. Mod. Phys.} \BibDash
\newblock 2003. \BibDash
\newblock Vol.~75. \BibDash
\newblock P.~473--541.

\bibitem{LeeRIXS}
\selectlanguageifdefined{english}
Asymmetry of collective excitations in electron- and hole-doped cuprate
  superconductors [Text]~/ W.~S.~Lee, J.~J.~Lee, E.~A.~Nowadnick [et~al.]~//
  \BibEmph{Nature Physics}. \BibDash
\newblock 2014. \BibDash
\newblock Vol.~10. \BibDash
\newblock P.~883.

\bibitem{ReznikNeutron}
\selectlanguageifdefined{english}
Local-moment fluctuations in the optimally doped high-tc superconductor
  $\mathrm{YBa_2Cu_3O_{6.95}}$ [Text]~/ D.~Reznik, J.~P.~Ismer, I.~Eremin
  [et~al.]~// \BibEmph{Phys. Rev. B}. \BibDash
\newblock 2008. \BibDash
\newblock Vol.~78. \BibDash
\newblock P.~132503.

\bibitem{Wilson}
\selectlanguageifdefined{english}
High-energy spin excitations in the electron-doped superconductor $\mathrm{
  Pr_{0.88}LaCe_{0.12}CuO_4}$ with $\mathrm{T_c=21 K}$ [Text]~/ S.~D.~Wilson,
  S.~Li, H.~Woo [et~al.]~// \BibEmph{Phys. Rev. Lett.} \BibDash
\newblock 2006. \BibDash
\newblock Vol.~96. \BibDash
\newblock P.~157001.

\bibitem{IshihNagaosa_PRB_2004}
\selectlanguageifdefined{english}
\BibEmph{Ishihara,~S.} Interplay of electron-phonon interaction and electron
  correlation in high-temperature superconductivity [Text]~/ Sumio~Ishihara,
  Naoto~Nagaosa~// \BibEmph{Phys. Rev. B.} \BibDash
\newblock 2004. \BibDash
\newblock Vol.~69. \BibDash
\newblock P.~144520.

\bibitem{Ovchinnikov}
\selectlanguageifdefined{russian}
\BibEmph{����������,~�.~�.} ����������� ������������ ��� ����-�������� � ������
  ��������-��������� �������������� � ������ ������� ����������.
  [{\cyr\CYRT\cyre\cyrk\cyrs\cyrt}]~/ �.~�.~����������, �.~�.~�������~//
  \BibEmph{����}. \BibDash
\newblock 2005. \BibDash
\newblock \CYRT. 128. \BibDash
\newblock {\cyr\CYRS.}~974.

\bibitem{PashitskiiColoumb}
\selectlanguageifdefined{english}
\BibEmph{Pashitskii,~E.~A.} On the plasmon mechanism of high-$\mathrm{T_c}$
  superconductivity in layered crystals and two-dimensional systems [Text]~/
  E.~A.~Pashitskii, V.~I.~Pentegov~// \BibEmph{Low Temperature Phys.} \BibDash
\newblock 2008. \BibDash
\newblock Vol.~34. \BibDash
\newblock P.~113.

\bibitem{TachikiColoumb}
\selectlanguageifdefined{english}
\BibEmph{Tachiki,~M.} Vibronic mechanism of high-${T}_{c}$ superconductivity
  [Text]~/ M.~Tachiki, M.~Machida, T.~Egami~// \BibEmph{Phys. Rev. B}. \BibDash
\newblock 2003. \BibDash
\newblock Vol.~67. \BibDash
\newblock P.~174506.

\bibitem{ErMalkhPRB2015}
\selectlanguageifdefined{english}
Pairing symmetry of the one-band hubbard model in the paramagnetic
  weak-coupling limit: A numerical $\mathrm{RPA}$ study [Text]~/ A.~T.~Romer,
  A.~Kreisel, I.~Eremin, M. A. Malakhov,  T. A. Maier, P. J. Hirschfeld, B. M. Andersen ~// \BibEmph{Phys. Rev. B}. \BibDash
\newblock 2015. \BibDash
\newblock Vol.~92. \BibDash
\newblock P.~104505.

\bibitem{bib:ErMalSun_JETPL_2012}
\selectlanguageifdefined{russian}
\BibEmph{������,~�.~�.} �������������� ���������� ���� ��-�� �������������� �
  �������� � $\mathrm{Bi_2Sr_2CaCuO_8}$ � ����������� ����������
  ��������������� ���� �� ������������� ����������� ��������� ����������������
  ���� � $\mathrm{YBa_2Cu_3O_7}$ [{\cyr\CYRT\cyre\cyrk\cyrs\cyrt}]~/
  �.~�.~������, �.~�.~�������, �.~�.~������~// \BibEmph{������ � ����}.
  \BibDash
\newblock 2012. \BibDash
\newblock \CYRT.~96. \BibDash
\newblock {\cyr\CYRS.}~110.

\bibitem{ErMalkh2014}
\selectlanguageifdefined{russian}
\BibEmph{������,~�.~�.} ����������� ����������� �������������� ���������� �
  �������� [{\cyr\CYRT\cyre\cyrk\cyrs\cyrt}]~/ �.~�.~������, �.~�.~�������~//
  \BibEmph{�������� ���. ����� ����������}. \BibDash
\newblock 2014. \BibDash
\newblock \CYRT.~78. \BibDash
\newblock {\cyr\CYRS.}~1183.

\bibitem{ErMalkhJETP2014}
\selectlanguageifdefined{russian}
\BibEmph{������,~�.~�.} ������������ ��������� ��������������� � ���������
  ���������� �������� ��� � �������� [{\cyr\CYRT\cyre\cyrk\cyrs\cyrt}]~/
  �.~�.~������, �.~�.~�������~// \BibEmph{������ � ����}. \BibDash
\newblock 2014. \BibDash
\newblock \CYRT. 100. \BibDash
\newblock {\cyr\CYRS.}~362--365.

\bibitem{ErMal2016}
\selectlanguageifdefined{russian}
\BibEmph{������,~�.~�.} � ������������ �������� ������������ � ��������� ���� �
  ����������� ������������ [{\cyr\CYRT\cyre\cyrk\cyrs\cyrt}]~/ �.~�.~������,
  �.~�.~�������~// \BibEmph{������ � ����}. \BibDash
\newblock 2016. \BibDash
\newblock \CYRT. 104. \BibDash
\newblock {\cyr\CYRS.}~13.

\bibitem{ErMal2017}
\selectlanguageifdefined{russian}
\BibEmph{������,~�.~�.} � ��������� ��������������� ���� � �������-������������
  ��������. [{\cyr\CYRT\cyre\cyrk\cyrs\cyrt}]~/ �.~�.~������, �.~�.~�������~//
  \BibEmph{������ � ����}. \BibDash
\newblock 2017. \BibDash
\newblock \CYRT. 105. \BibDash
\newblock {\cyr\CYRS.}~6??

\bibitem{Scalapino1999}
\selectlanguageifdefined{english}
\BibEmph{Scalapino,~D.~J.} Superconductivity and spin fluctuations [Text]~/
  D.~J.~Scalapino~// \BibEmph{J. Low Temp. Phys.} \BibDash
\newblock 1999. \BibDash
\newblock Vol. 117. \BibDash
\newblock P.~179.

\bibitem{Scalapino1986}
\selectlanguageifdefined{english}
\BibEmph{Scalapino,~D.~J.} d-wave pairing near a spin-density-wave instability
  [Text]~/ D.~J.~Scalapino, E.~Loh, J.~E.~Hirsch~// \BibEmph{Phys. Rev. B}.
  \BibDash
\newblock 1986. \BibDash
\newblock Vol.~34. \BibDash
\newblock P.~8190--8192.

\bibitem{Monod1986}
\selectlanguageifdefined{english}
\BibEmph{Beal-Monod,~M.~T.} Possible superconductivity in nearly
  antiferromagnetic itinerant fermion systems [Text]~/ M.~T.~Beal-Monod,
  C.~Bourbonnais, V.~J.~Emery~// \BibEmph{Phys. Rev. B}. \BibDash
\newblock 1986. \BibDash
\newblock Vol.~34. \BibDash
\newblock P.~7716.

\bibitem{Miyake1986}
\selectlanguageifdefined{english}
\BibEmph{Miyake,~K.} Spin-fluctuation-mediated even-parity pairing in
  heavy-fermion superconductors [Text]~/ K.~Miyake, S.~Schmitt-Rink,
  C.~M.~Varma~// \BibEmph{Phys. Rev. B}. \BibDash
\newblock 1986. \BibDash
\newblock Vol.~34. \BibDash
\newblock P.~6554.

\bibitem{Kagan1989}
\selectlanguageifdefined{russian}
\BibEmph{�����,~�.~�.} ��������� ����������� ������������� �������� �
  �������������� �����-���� � ������������� [{\cyr\CYRT\cyre\cyrk\cyrs\cyrt}]~/
  �.~�.~�����, �.~�.~�������~// \BibEmph{������ � ����}. \BibDash
\newblock 1989. \BibDash
\newblock \CYRT.~50. \BibDash
\newblock {\cyr\CYRS.}~483.

\bibitem{Chubukov1993}
\selectlanguageifdefined{english}
\BibEmph{Chubukov,~A.~V.} Pairing instabilities in the two-dimensional hubbard
  model [Text]~/ A.~V.~Chubukov, J.~P.~Lu~// \BibEmph{Phys. Rev. B}. \BibDash
\newblock 1993. \BibDash
\newblock Vol.~46. \BibDash
\newblock P.~11163.

\bibitem{Zanchi1996}
\selectlanguageifdefined{english}
\BibEmph{Zanchi,~D.} Superconducting instabilities of the non-half-filled
  hubbard model in two dimensions [Text]~/ D.~Zanchi, H.~J.~Schulz~//
  \BibEmph{Phys. Rev. B}. \BibDash
\newblock 1996. \BibDash
\newblock Vol.~54. \BibDash
\newblock P.~9509.

\bibitem{Halboth2000}
\selectlanguageifdefined{english}
\BibEmph{Halboth,~C.~J.} d-wave superconductivity and pomeranchuk instability
  in the two-dimensional hubbard model [Text]~/ C.~J.~Halboth, W.~Metzner~//
  \BibEmph{Phys. Rev. Lett.} \BibDash
\newblock 2000. \BibDash
\newblock Vol.~85. \BibDash
\newblock P.~5162.

\bibitem{Honerkamp2001}
\selectlanguageifdefined{english}
\BibEmph{Honerkamp,~C.} Magnetic and superconducting instabilities of the
  hubbard model at the van hove filling [Text]~/ C.~Honerkamp, M.~Salmhofer~//
  \BibEmph{Phys. Rev. Lett.} \BibDash
\newblock 2001. \BibDash
\newblock Vol.~87. \BibDash
\newblock P.~187004.

\bibitem{Raghu2010}
\selectlanguageifdefined{english}
\BibEmph{Raghu,~S.} Superconductivity in the repulsive hubbard model: An
  asymptotically exact weak-coupling solution [Text]~/ S.~Raghu,
  S.~A.~Kivelson, D.~J.~Scalapino~// \BibEmph{Phys. Rev. B}. \BibDash
\newblock 2010. \BibDash
\newblock Vol.~81. \BibDash
\newblock P.~224505.

\bibitem{Hlubina1999}
\selectlanguageifdefined{english}
\BibEmph{Hlubina,~R.} Phase diagram of the weak-coupling two-dimensional
  $t\ensuremath{-}{t}^{\ensuremath{'}}$ hubbard model at low and intermediate
  electron density [Text]~/ R.~Hlubina~// \BibEmph{Phys. Rev. B}. \BibDash
\newblock 1999. \BibDash
\newblock Vol.~59. \BibDash
\newblock P.~9600.

\bibitem{Raghu2012}
\selectlanguageifdefined{english}
Effects of longer-range interactions on unconventional superconductivity
  [Text]~/ S.~Raghu, E.~Berg, A.~V.~Chubukov, S.~A.~Kivelson~// \BibEmph{Phys.
  Rev. B}. \BibDash
\newblock 2012. \BibDash
\newblock Vol.~85. \BibDash
\newblock P.~024516.

\bibitem{Armitage2010}
\selectlanguageifdefined{english}
\BibEmph{Armitage,~N.~P.} Progress and perspectives on electron-doped cuprates
  [Text]~/ N.~P.~Armitage, P.~Fournier, R.~L.~Greene~// \BibEmph{Rev. Mod.
  Phys.} \BibDash
\newblock 2010. \BibDash
\newblock Vol.~82. \BibDash
\newblock P.~2421.

\bibitem{Hashimoto2014}
\selectlanguageifdefined{english}
Energy gaps in high-transition-temperature cuprate superconductors [Text]~/
  M.~Hashimoto, I.~M.~Vishik, R.-H.~He [et~al.]~// \BibEmph{Nat. Phys.}
  \BibDash
\newblock 2014. \BibDash
\newblock Vol.~10. \BibDash
\newblock P.~483.

\bibitem{BlumbergGAP}
\selectlanguageifdefined{english}
Nonmonotonic $d_{x2-y2}$ superconducting order parameter in
  $\mathrm{Nd_{2-x}Ce_xCuO_4}$ [Text]~/ G.~Blumberg, A.~Koitzsch, A.~Gozar
  [et~al.]~// \BibEmph{Phys. Rev. Lett.} \BibDash
\newblock 2002. \BibDash
\newblock Vol.~88. \BibDash
\newblock P.~107002.

\bibitem{MatsuiGAP}
\selectlanguageifdefined{english}
Angle-resolved photoemission spectroscopy of the antiferromagnetic
  superconductor $\mathrm{Nd_{1.87}Ce_{0.13}CuO_4}$: Anisotropic
  spin-correlation gap, pseudogap, and the induced quasiparticle mass
  enhancement [Text]~/ H.~Matsui, K.~Terashima, T.~Sato [et~al.]~//
  \BibEmph{Phys. Rev. Lett.} \BibDash
\newblock 2005. \BibDash
\newblock Vol.~94. \BibDash
\newblock P.~047005.

\bibitem{Terashima2007}
\selectlanguageifdefined{english}
Anomalous momentum dependence of the superconducting coherence peak and its
  relation to the pseudogap of $\mathrm{La_{1.85}Sr_{0.15}CuO_4}$ [Text]~/
  K.~Terashima, H.~Matsui, T.~Sato [et~al.]~// \BibEmph{Phys. Rev. Lett.}
  \BibDash
\newblock 2007. \BibDash
\newblock Vol.~99. \BibDash
\newblock P.~017003.

\bibitem{Deng2015}
\selectlanguageifdefined{english}
Emergent $\mathrm{BCS}$ regime of the two-dimensional fermionic hubbard model:
  Ground state phase diagram [Text]~/ Y.~Deng, E.~Kozik, N.~V.~Prokofev,
  B.~V.~Svistunov~// \BibEmph{Europhys. Lett.} \BibDash
\newblock 2015. \BibDash
\newblock Vol. 110. \BibDash
\newblock P.~57001.

\bibitem{Katanin2003}
\selectlanguageifdefined{english}
\BibEmph{Katanin,~A.~A.} Renormalization group analysis of magnetic and
  superconducting instabilities near van hove band fillings [Text]~/
  A.~A.~Katanin, A.~P.~Kampf~// \BibEmph{Phys. Rev. B}. \BibDash
\newblock 2003. \BibDash
\newblock Vol.~68. \BibDash
\newblock P.~195101.

\bibitem{ErLar1995}
\selectlanguageifdefined{russian}
\BibEmph{������,~�.~�.} ��������� �������������� ���� � �������� ��������.
  ������ ��������. [{\cyr\CYRT\cyre\cyrk\cyrs\cyrt}]~/ �.~�.~������,
  �.~�.~��������~// \BibEmph{������ � ����}. \BibDash
\newblock 1995. \BibDash
\newblock \CYRT.~62. \BibDash
\newblock {\cyr\CYRS.}~192.

\bibitem{ScalapinoHubb}
\selectlanguageifdefined{english}
\BibEmph{Scalapino,~D.~J.} Numerical studies of the $2D$ Hubbard model, in
  Handbook of High-Temperature Superconductivity [Text]~/ D.~J.~Scalapino.
  \BibDash
\newblock New York~: Springer, 2007. \BibDash
\newblock P.~495--526.

\bibitem{Gull2013}
\selectlanguageifdefined{english}
\BibEmph{Gull,~E.} Superconductivity and the pseudogap in the two-dimensional
  hubbard model [Text]~/ E.~Gull, O.~Parcollet, A.~J.~Millis~// \BibEmph{Phys.
  Rev. Lett.} \BibDash
\newblock 2013. \BibDash
\newblock Vol. 110. \BibDash
\newblock P.~216405.

\bibitem{Staar2014}
\selectlanguageifdefined{english}
\BibEmph{Staar,~P.} Two-particle correlations in a dynamic cluster
  approximation with continuous momentum dependence: Superconductivity in the
  two-dimensional hubbard model [Text]~/ P.~Staar, T.~Maier,
  T.~C.~Schulthess~// \BibEmph{Phys. Rev. B}. \BibDash
\newblock 2014. \BibDash
\newblock Vol.~89. \BibDash
\newblock P.~195133.

\bibitem{Xiao2016}
\selectlanguageifdefined{english}
\BibEmph{Zheng,~B.-X.} Ground-state phase diagram of the square lattice hubbard
  model from density matrix embedding theory [Text]~/ Bo-Xiao~Zheng, Garnet
  Kin-Lic~Chan~// \BibEmph{Phys. Rev. B}. \BibDash
\newblock 2016. \BibDash
\newblock Vol.~93. \BibDash
\newblock P.~035126.

\bibitem{BerkSchrieffer}
\selectlanguageifdefined{english}
\BibEmph{Berk,~N.~F.} Effect of ferromagnetic spin correlations on
  superconductivity [Text]~/ N.~F.~Berk, J.~R.~Schrieffer~// \BibEmph{Phys.
  Rev. Lett.} \BibDash
\newblock 1966. \BibDash
\newblock Vol.~17. \BibDash
\newblock P.~433.

\bibitem{Guinea2004}
\selectlanguageifdefined{english}
\BibEmph{Guinea,~F.} Superconductivity in electron-doped cuprates: Gap shape
  change and symmetry crossover with doping [Text]~/ Francisco~Guinea,
  Robert~S.~Markiewicz, Mar\'{\i}a A.~H.~Vozmediano~// \BibEmph{Phys. Rev. B}.
  \BibDash
\newblock 2004. \BibDash
\newblock Vol.~69. \BibDash
\newblock P.~054509.

\bibitem{Parker2008}
\selectlanguageifdefined{english}
\BibEmph{Parker,~D.} Quantitative evidence for spin-fluctuation-mediated
  higher-harmonic $d$-wave components: Hole- and electron-doped cuprates
  [Text]~/ David~Parker, Alexander~V.~Balatsky~// \BibEmph{Phys. Rev. B}.
  \BibDash
\newblock 2008. \BibDash
\newblock Vol.~78. \BibDash
\newblock P.~214502.

\bibitem{Khodel2004}
\selectlanguageifdefined{english}
Hot spots and transition from d-wave to another pairing symmetry in the
  electron-doped cuprate superconductors [Text]~/ V.~A.~Khodel,
  Victor~M.~Yakovenko, M.~V.~Zverev, Haeyong~Kang~// \BibEmph{Phys. Rev. B}.
  \BibDash
\newblock 2004. \BibDash
\newblock Vol.~69. \BibDash
\newblock P.~144501.

\bibitem{Krotkov2006}
\selectlanguageifdefined{english}
\BibEmph{Krotkov,~P.} Non-fermi liquid and pairing in electron-doped cuprates
  [Text]~/ P.~Krotkov, Andrey~V.~Chubukov~// \BibEmph{Phys. Rev. Lett.}
  \BibDash
\newblock 2006. \BibDash
\newblock Vol.~96. \BibDash
\newblock P.~107002.

\bibitem{EreminTsoncheva2008}
\selectlanguageifdefined{english}
\BibEmph{Eremin,~I.} Signature of the nonmonotonic $d$-wave gap in
  electron-doped cuprates [Text]~/ Ilya~Eremin, Evelina~Tsoncheva,
  Andrey~V.~Chubukov~// \BibEmph{Phys. Rev. B}. \BibDash
\newblock 2008. \BibDash
\newblock Vol.~77. \BibDash
\newblock P.~024508.

\bibitem{Yoshimura2004}
\selectlanguageifdefined{english}
\BibEmph{Yoshimura,~H.} Angular dependence of the superconducting order
  parameter in electron-doped high-temperature superconductors [Text]~/
  H.~Yoshimura, D.~S.~Hirashima~// \BibEmph{J. Phys. Soc. Jpn.} \BibDash
\newblock 2004. \BibDash
\newblock Vol.~73. \BibDash
\newblock P.~2057.

\bibitem{GinsHTSC}
\selectlanguageifdefined{russian}
\BibEmph{��������,~�.~�.} �������� ������������������� �����������������
  [{\cyr\CYRT\cyre\cyrk\cyrs\cyrt}]~/ �.~�.~��������, �.~�.~�������. \BibDash
\newblock ������~: �����, 1977.

\bibitem{LeggettColoumb}
\selectlanguageifdefined{english}
\BibEmph{Leggett,~A.~J.} Cuprate superconductivity: Dependence of $t_c$ on the
  c-axis layering structure [Text]~/ A.~J.~Leggett~// \BibEmph{Phys. Rev. Let.}
  \BibDash
\newblock 1999. \BibDash
\newblock Vol.~83. \BibDash
\newblock P.~392.

\bibitem{bib:Mahan}
\selectlanguageifdefined{english}
\BibEmph{Mahan,~G.~D.} Many-Particle Physics [Text]~/ G.~D.~Mahan. \BibDash
\newblock New York~: Plenum Press, 1990.

\bibitem{PinesNozieres}
\selectlanguageifdefined{russian}
\BibEmph{�����,~�.} ������ ��������� ���������
  [{\cyr\CYRT\cyre\cyrk\cyrs\cyrt}]~/ �.~�����, �.~������. \BibDash
\newblock ������~: ���, 1967.

\bibitem{bib:Markiewicz2011}
\selectlanguageifdefined{english}
Competing phases in the cuprates: Charge vs spin order [Text]~/
  R.~S.~Markiewicz, J.~Lorenzana, G.~Seibold, A.~Bansil~// \BibEmph{Journal of
  Physics and Chemistry of Solids}. \BibDash
\newblock 2011. \BibDash
\newblock Vol.~72. \BibDash
\newblock P.~333.

\bibitem{bib:Norman2014}
\selectlanguageifdefined{english}
\BibEmph{Melikyan,~A.} Symmetry of the charge density wave in cuprates [Text]~/
  Ashot~Melikyan, M.~R.~Norman~// \BibEmph{Phys. Rev. B.} \BibDash
\newblock 2014. \BibDash
\newblock Vol.~89. \BibDash
\newblock P.~024507.

\bibitem{bib:ErErVarl2001}
\selectlanguageifdefined{english}
\BibEmph{Eremin,~M.} Dynamical charge susceptibility in layered cuprates:
  Beyond the conventional random-phase-approximation scheme [Text]~/ M.~Eremin,
  I.~Eremin, S.~Varlamov~// \BibEmph{Phys. Rev. B}. \BibDash
\newblock 2001. \BibDash
\newblock Vol.~64. \BibDash
\newblock P.~214512.

\bibitem{ESEEPJ2012}
\selectlanguageifdefined{english}
\BibEmph{Eremin,~M.~V.} Dual features of magnetic susceptibility in
  superconducting cuprates: a comparison to inelastic neutron scattering
  [Text]~/ M.~V.~Eremin, I.~M.~Shigapov, I.~M.~Eremin~// \BibEmph{Eur. Phys. J.
  B.} \BibDash
\newblock 2012. \BibDash
\newblock Vol.~85. \BibDash
\newblock P.~131.

\bibitem{BeccaGrilli_PRB_2003}
\selectlanguageifdefined{english}
Charge-density waves and superconductivity as an alternative to phase
  separation in the infinite-u hubbard-holstein model [Text]~/ F.~Becca,
  M.~Tarquini, M.~Grilli, C.~Di~Castro~// \BibEmph{Phys. Rev. B.} \BibDash
\newblock 1996. \BibDash
\newblock Vol.~54. \BibDash
\newblock P.~12443.

\bibitem{LanzaraKink}
\selectlanguageifdefined{english}
Evidence for ubiquitous strong electron-phonon coupling in high-temperature
  superconductors [Text]~/ A.~Lanzara, P.~V.~Bogdanov, X.~J.~Zhou [et~al.]~//
  \BibEmph{Nature}. \BibDash
\newblock 2001. \BibDash
\newblock Vol. 412. \BibDash
\newblock P.~510.

\bibitem{IwasawaKink}
\selectlanguageifdefined{english}
\BibEmph{Iwasawa,~H.} Isotopic fingerprint of electron-phonon coupling in
  high-$t_c$ cuprates [Text]~/ H.~Iwasawa, J.~F.~Douglas, K.~Sato~//
  \BibEmph{Phys. Rev. Lett.} \BibDash
\newblock 2008. \BibDash
\newblock Vol. 101. \BibDash
\newblock P.~157005.

\bibitem{CukKink}
\selectlanguageifdefined{english}
\BibEmph{Cuk,~T.} Coupling of the b1g phonon to the antinodal electronic states
  of $\mathrm{Bi_2Sr_2CaCu_2O_{8-x}}$ [Text]~/ T.~Cuk, F.~Baumberger,
  D.~H.~Lu~// \BibEmph{Phys. Rev. Lett.} \BibDash
\newblock 2004. \BibDash
\newblock Vol.~93. \BibDash
\newblock P.~117003.

\bibitem{Norman_PRB_2007}
\selectlanguageifdefined{english}
\BibEmph{Norman,~M.~R.} Linear response theory and the universal nature of the
  magnetic excitation spectrum of the cuprates [Text]~/ M.~R.~Norman~//
  \BibEmph{Phys. Rev. B.} \BibDash
\newblock 2007. \BibDash
\newblock Vol.~75. \BibDash
\newblock P.~184514.

\bibitem{EreminNaturforsh}
\selectlanguageifdefined{english}
\BibEmph{Eremin,~M.~V.} Why is the $\mathrm{Cu(2) Asymmetry Parameter of NQR
  Spectra in HTSC}$ so small? [Text]~/ M.~V.~Eremin~// \BibEmph{Naturforsh}.
  \BibDash
\newblock 1994. \BibDash
\newblock Vol.~49. \BibDash
\newblock P.~385.

\bibitem{SongNu1}
\selectlanguageifdefined{english}
\BibEmph{Song}. Electron-phonon coupling and d-wave superconductivity in the
  cuprates [Text]~/ ~Song, J.~F.~Annett~// \BibEmph{Phys. Rev. B}. \BibDash
\newblock 1995. \BibDash
\newblock Vol.~51. \BibDash
\newblock P.~3840.

\bibitem{SandvikNu1}
\selectlanguageifdefined{english}
\BibEmph{Sandvik,~W.} Effect of an electron-phonon interaction on the
  one-electron spectral weight of a d-wave superconductor [Text]~/ W.~Sandvik,
  D.~J.~Scalapino, N.~E.~Bickers~// \BibEmph{Phys. Rev. B}. \BibDash
\newblock 2004. \BibDash
\newblock Vol.~69. \BibDash
\newblock P.~094523.

\bibitem{RoschNu1}
\selectlanguageifdefined{english}
\BibEmph{Rosch,~O.} Electron-phonon interaction in the t-j model [Text]~/
  O.~Rosch, O.~Gunnarsson~// \BibEmph{Phys. Rev. Lett.} \BibDash
\newblock 2004. \BibDash
\newblock Vol.~92. \BibDash
\newblock P.~146403.

\bibitem{ErErLar2002}
\selectlanguageifdefined{english}
Influence of polaronic effects on superexchange interaction: Isotope effects of
  $t_n$, $t*$, and $t_c$ in layered cuprates [Text]~/ M.~V.~Eremin,
  I.~M.~Eremin, I.~A.~Larionov, A.~F.~Terzi~// \BibEmph{JETP Letters}. \BibDash
\newblock 2002. \BibDash
\newblock Vol.~75. \BibDash
\newblock P.~395.

\bibitem{OvShn2009}
\selectlanguageifdefined{russian}
\BibEmph{�������,~�.~�.} ������������� ������ � ������ ������ ���������������
  ����������, ����������� ��������� � �������� ��������� ����������������
  ���������� [{\cyr\CYRT\cyre\cyrk\cyrs\cyrt}]~/ �.~�.~�������,
  �.~�.����������~// \BibEmph{����}. \BibDash
\newblock 2009. \BibDash
\newblock \CYRT.~6. \BibDash
\newblock {\cyr\CYRS.}~1177.

\bibitem{NuckerPlasmons}
\selectlanguageifdefined{english}
Long-wavelength collective excitations of charge carriers in
  high-${\mathit{t}}_{\mathit{c}}$ superconductors [Text]~/ N.~N\"ucker,
  U.~Eckern, J.~Fink, P.~M\"uller~// \BibEmph{Phys. Rev. B}. \BibDash
\newblock 1991. \BibDash
\newblock Vol.~44. \BibDash
\newblock P.~7155.

\bibitem{Norman_PRB_2003}
\selectlanguageifdefined{english}
\BibEmph{Norman,~M.~R.} Magnetic collective mode dispersion in high-temperature
  superconductors [Text]~/ M.~R.~Norman~// \BibEmph{Phys. Rev. B.} \BibDash
\newblock 2003. \BibDash
\newblock Vol.~63. \BibDash
\newblock P.~092509.

\bibitem{BillRPA}
\selectlanguageifdefined{english}
\BibEmph{Bill,~A.} Electronic collective modes and superconductivity in layered
  conductors [Text]~/ A.~Bill, H.~Morawitz, V.~Z.~Kresin~// \BibEmph{Phys. Rev.
  B.} \BibDash
\newblock 2003. \BibDash
\newblock Vol.~68. \BibDash
\newblock P.~144519.

\bibitem{EreminMetodichka}
\selectlanguageifdefined{russian}
\BibEmph{������,~�.~�.} ���������������� ������ � ���������������� ������
  [{\cyr\CYRT\cyre\cyrk\cyrs\cyrt}]~/ �.~�.~������. \BibDash
\newblock ������~: ��������� (�����������) ����������� �����������, 2011.

\bibitem{Reznik_2010}
\selectlanguageifdefined{english}
\BibEmph{Reznik,~D.} Giant electron-phonon anomaly in doped and other cuprates
  [Text]~/ D.~Reznik~// \BibEmph{Advances in Condensed Matter Phys.} \BibDash
\newblock 2010. \BibDash
\newblock Vol. 2010. \BibDash
\newblock P.~523549.

\bibitem{Wang_PRB_1990}
\selectlanguageifdefined{english}
\BibEmph{Wang,~Y.-Y.} Electron-energy-loss and optical-transmittance
  investigation of
  ${\mathrm{bi}}_{2}$${\mathrm{sr}}_{2}$${\mathrm{cacu}}_{2}$${\mathrm{o}}_{8}$
  [Text]~/ Yun-Yu~Wang, Goufu~Feng, A.~L.~Ritter~// \BibEmph{Phys. Rev. B.}
  \BibDash
\newblock 1990. \BibDash
\newblock Vol.~42. \BibDash
\newblock P.~420.

\bibitem{Rice2009}
\selectlanguageifdefined{english}
Nature of stripes in the generalized t � j model applied to the cuprate
  superconductors [Text]~/ Kai-Yu~Yang, Wei~Qiang~Chen, T~M~Rice [et~al.]~//
  \BibEmph{New Journal of Physics}. \BibDash
\newblock 2009. \BibDash
\newblock Vol.~11. \BibDash
\newblock P.~055053.

\bibitem{Peng2016}
\selectlanguageifdefined{english}
Direct observation of charge order in underdoped and optimally doped
  ${\mathrm{bi}}_{2}{(\mathrm{Sr},\mathrm{La})}_{2}{\mathrm{cuo}}_{6+\ensuremath{\delta}}$
  by resonant inelastic x-ray scattering [Text]~/ Y.~Y.~Peng, M.~Salluzzo,
  X.~Sun [et~al.]~// \BibEmph{Phys. Rev. B}. \BibDash
\newblock 2016. \BibDash
\newblock Vol.~94. \BibDash
\newblock P.~184511.

\bibitem{bib:ErShigHo}
\selectlanguageifdefined{english}
\BibEmph{Eremin,~M.~V.} Collective spin excitations in the singlet-correlated
  band model: a comparison with resonant inelastic x-ray scattering [Text]~/
  M.~V.~Eremin, I.~M.~Shigapov, Ho~Thi~Duyen~Thuy~// \BibEmph{J. Phys.:
  Condens. Matter}. \BibDash
\newblock 2013. \BibDash
\newblock Vol.~25. \BibDash
\newblock P.~345701.

\bibitem{BraicovichRIXS}
\selectlanguageifdefined{english}
Magnetic excitations and phase separation in the underdoped
  $\mathrm{La_{2-x}Sr_xCuO_4}$ superconductor measured by resonant inelastic
  x-ray scattering [Text]~/ L.~Braicovich, J.~Brink, V.~Bisogni [et~al.]~//
  \BibEmph{Phys. Rev. Lett.} \BibDash
\newblock 2010. \BibDash
\newblock Vol. 104. \BibDash
\newblock P.~077002.

\bibitem{TaconRIXS}
\selectlanguageifdefined{english}
Intense paramagnon excitations in a large family of high-temperature
  superconductors [Text]~/ M.~Le~Tacon, G.~Ghiringhelli, J.~Chaloupka
  [et~al.]~// \BibEmph{Nature Physics}. \BibDash
\newblock 2011. \BibDash
\newblock Vol.~7. \BibDash
\newblock P.~725.

\bibitem{DeanRIXS}
\selectlanguageifdefined{english}
Persistence of magnetic excitations in $\mathrm{La_{2-x}Sr_xCuO_4}$ from the
  undoped insulator to the heavily overdoped non-superconducting metal [Text]~/
  M.~P.~M.~Dean, G.~Dellea, R.~S.~Springell [et~al.]~// \BibEmph{Nat. Mater.}
  \BibDash
\newblock 2013. \BibDash
\newblock Vol.~12. \BibDash
\newblock P.~1019.

\bibitem{GuariseAnisSoft}
\selectlanguageifdefined{english}
Anisotropic softening of magnetic excitations along the nodal direction in
  superconducting cuprates [Text]~/ M.~Guarise, B.~Dalla~Piazza, H.~Berger
  [et~al.]~// \BibEmph{Nature Communications}. \BibDash
\newblock 2014. \BibDash
\newblock Vol.~5. \BibDash
\newblock P.~5760.

\bibitem{JiaDQMC}
\selectlanguageifdefined{english}
Persistent spin excitations in doped antiferromagnets revealed by resonant
  inelastic light scattering [Text]~/ C.~J.~Jia, E.~A.~Nowadnick, K.~Wohlfeld
  [et~al.]~// \BibEmph{Nature communication}. \BibDash
\newblock 2014. \BibDash
\newblock Vol.~5. \BibDash
\newblock P.~3314.

\bibitem{AndersontJ}
\selectlanguageifdefined{english}
\BibEmph{Anderson,~P.~W.} The resonating valence bond state in
  $\mathrm{La_2CuO_4}$ and superconductivity [Text]~/ P.~W.~Anderson~//
  \BibEmph{Science}. \BibDash
\newblock 1987. \BibDash
\newblock Vol. 235. \BibDash
\newblock P.~1196.

\bibitem{IzumovtJ}
\selectlanguageifdefined{russian}
\BibEmph{������,~�.~�.} ������ ��������������� ���������: t-j-������
  [{\cyr\CYRT\cyre\cyrk\cyrs\cyrt}]~/ �.~�.~������~// \BibEmph{������
  ���������� ����}. \BibDash
\newblock 1997. \BibDash
\newblock \CYRT. 167. \BibDash
\newblock {\cyr\CYRS.}~465.

\bibitem{Valkov3c}
\selectlanguageifdefined{russian}
\BibEmph{�������,~�.~�.} ����������� ������ � ����������� ����������������
  �������� ��������������������� ��������� � �������������� ����������������
  [{\cyr\CYRT\cyre\cyrk\cyrs\cyrt}]~/ �.~�.~�������, �.~�.~������������~//
  \BibEmph{����}. \BibDash
\newblock 2005. \BibDash
\newblock \CYRT. 127. \BibDash
\newblock {\cyr\CYRS.}~686.

\bibitem{Korshunov3c}
\selectlanguageifdefined{russian}
\BibEmph{��������,~�.~�.} ����������� ������������ � �������� ���������� �
  ��������������� ��� �������� n-���� [{\cyr\CYRT\cyre\cyrk\cyrs\cyrt}]~/
  �.~�.~��������, �.~�.~����������, �.~�.~������~// \BibEmph{������ � ����}.
  \BibDash
\newblock 2004. \BibDash
\newblock \CYRT.~80. \BibDash
\newblock {\cyr\CYRS.}~45.

\bibitem{Wang3c}
\selectlanguageifdefined{english}
Origin of strong dispersion in hubbard insulators [Text]~/ Y.~Wang,
  K.~Wohlfeld, B.~Moritz [et~al.]~// \BibEmph{Phys. Rev. B}. \BibDash
\newblock 2015. \BibDash
\newblock Vol.~92. \BibDash
\newblock P.~075119.

\bibitem{ESEMagnRes2014}
\selectlanguageifdefined{english}
\BibEmph{Eremin,~M.~V.} Spin response in $\mathrm{HTSC}$ cuprates: generalized
  $\mathrm{RPA}$ approach with projection operators method [Text]~/
  M.~V.~Eremin, I.~M.~Shigapov, I.M.~Eremin~// \BibEmph{Magnetic Resonance in
  Solids. Electronic Journal.} \BibDash
\newblock 2014. \BibDash
\newblock Vol.~16. \BibDash
\newblock P.~14206.

\bibitem{Korsh3c}
\selectlanguageifdefined{russian}
\BibEmph{��������,~�.~�.} ����������� ������������ � �������� ���������� �
  ��������������� ��� �������� n- ���� [{\cyr\CYRT\cyre\cyrk\cyrs\cyrt}]~/
  �.~�.~��������, �.~�.~����������, �.~�.~������~// \BibEmph{������ � ����}.
  \BibDash
\newblock 2004. \BibDash
\newblock \CYRT.~80. \BibDash
\newblock {\cyr\CYRS.}~45.

\bibitem{EAEJETP2007}
\selectlanguageifdefined{russian}
\BibEmph{�������,~�.~�.} � ������ ���������� ��������� ��������� �
  ��������������� $\mathrm{Pr_{0.88}LaCe_{0.12}CuO_{4-x}}$
  [{\cyr\CYRT\cyre\cyrk\cyrs\cyrt}]~/ �.~�.~�������, �.~�~������,
  �.~�.~������~// \BibEmph{������ � ����}. \BibDash
\newblock 2007. \BibDash
\newblock \CYRT.~86. \BibDash
\newblock {\cyr\CYRS.}~386.

\bibitem{EAEJETP2008}
\selectlanguageifdefined{russian}
\BibEmph{������,~�.~�.} ������������ �������� ��������������� �������� ���� �
  ������ ���������-��������������� ���� ������������
  [{\cyr\CYRT\cyre\cyrk\cyrs\cyrt}]~/ �.~�.~������, �.~�.~�����,
  �.~�.~������~// \BibEmph{����}. \BibDash
\newblock 2008. \BibDash
\newblock \CYRT. 113. \BibDash
\newblock {\cyr\CYRS.}~862.

\bibitem{EAEJETP2009}
\selectlanguageifdefined{russian}
\BibEmph{�������,~�.~�.} � ������ ������������ �������� ��������������� �
  ������ t-j-v ������. ������������� � ������� �� ��������� ��������� �
  $\mathrm{Pr_{0.88}LaCe_{0.12}CuO_{4-x}}$ � $\mathrm{La_{2-x}Sr_x CuO_4}$
  [{\cyr\CYRT\cyre\cyrk\cyrs\cyrt}]~/ �.~�.~�������, �.~�~������,
  �.~�.~������~// \BibEmph{����}. \BibDash
\newblock 2009. \BibDash
\newblock \CYRT. 135. \BibDash
\newblock {\cyr\CYRS.}~65.

\bibitem{ArmitageARPES}
\selectlanguageifdefined{english}
Doping dependence of an n-type cuprate superconductor investigated by
  angle-resolved photoemission spectroscopy [Text]~/ N.~P.~Armitage,
  F.~Ronning, D.~H.~Lu [et~al.]~// \BibEmph{Phys. Rev. Lett.} \BibDash
\newblock 2002. \BibDash
\newblock Vol.~88. \BibDash
\newblock P.~257001.

\bibitem{IsmerARPES}
\selectlanguageifdefined{english}
Magnetic resonance in the spin excitation spectrum of electron-doped cuprate
  superconductors [Text]~/ J.~P.~Ismer, I.~Eremin, E.~Rossi, D.~K.~Morr~//
  \BibEmph{Phys. Rev. Lett.} \BibDash
\newblock 2007. \BibDash
\newblock Vol.~99. \BibDash
\newblock P.~047005.

\bibitem{Wilson2}
\selectlanguageifdefined{english}
Evolution of low-energy spin dynamics in the electron-doped
  high-transition-temperature superconductor
  ${\mathrm{pr}}_{0.88}{\mathrm{lace}}_{0.12}{\mathrm{cuo}}_{4\ensuremath{-}\ensuremath{\delta}}$
  [Text]~/ Stephen~D.~Wilson, Shiliang~Li, Pengcheng~Dai [et~al.]~//
  \BibEmph{Phys. Rev. B}. \BibDash
\newblock 2006. \BibDash
\newblock Vol.~74. \BibDash
\newblock P.~144514.

\bibitem{MesotGapAng}
\selectlanguageifdefined{english}
Superconducting gap anisotropy and quasiparticle interactions: A doping
  dependent photoemission study [Text]~/ J.~Mesot, M.~R.~Norman, H.~Ding
  [et~al.]~// \BibEmph{Phys. Rev. Lett.} \BibDash
\newblock 1999. \BibDash
\newblock Vol.~83. \BibDash
\newblock P.~840--843.

\bibitem{BorisenkoGapAng}
\selectlanguageifdefined{english}
Superconducting gap in the presence of bilayer splitting in underdoped
  $(\mathrm{P}\mathrm{b},\mathrm{B}\mathrm{i}{)}_{2}{\mathrm{sr}}_{2}{\mathrm{cacu}}_{2}{\mathrm{o}}_{8+\ensuremath{\delta}}$
  [Text]~/ S.~V.~Borisenko, A.~A.~Kordyuk, T.~K.~Kim [et~al.]~// \BibEmph{Phys.
  Rev. B}. \BibDash
\newblock 2002. \BibDash
\newblock Vol.~66. \BibDash
\newblock P.~140509.

\bibitem{KohsakaGapAng}
\selectlanguageifdefined{english}
How cooper pairs vanish approaching the mott insulator in
  $\mathrm{Bi}_2\mathrm{Sr}_2\mathrm{Ca}\mathrm{Cu}_2\mathrm{O}_{8+\delta}$
  [Text]~/ Y.~Kohsaka, C.~Taylor, P.~Wahl [et~al.]~// \BibEmph{Nature}.
  \BibDash
\newblock 2008. \BibDash
\newblock Vol. 454. \BibDash
\newblock P.~1072--1078.

\bibitem{KaganHarm2014}
\selectlanguageifdefined{russian}
������ ����-���������� � ���������� ���������� � ����� ��������������� ��������
  � �������. [{\cyr\CYRT\cyre\cyrk\cyrs\cyrt}]~/ �.~�.~�����, �.�.~�������,
  �.~�.~������, �.~�.~����������~// \BibEmph{����}. \BibDash
\newblock 2014. \BibDash
\newblock \CYRT. 145. \BibDash
\newblock {\cyr\CYRS.}~1127.

\bibitem{BCSDahm}
\selectlanguageifdefined{english}
Strength of the spin-fluctuation-mediated pairing interaction in a
  high-temperature superconductor [Text]~/ T.~Dahm, V.~Hinkov, S.~V.~Borisenko
  [et~al.]~// \BibEmph{Nature Physics}. \BibDash
\newblock 2009. \BibDash
\newblock Vol.~5. \BibDash
\newblock P.~217--221.

\bibitem{PhononSneidOvc}
\selectlanguageifdefined{russian}
\BibEmph{�������,~�.~�.} �������� � ��������� ��������� ���������� �
  ������������������� ���������������� � ������ ������� ����������
  [{\cyr\CYRT\cyre\cyrk\cyrs\cyrt}]~/ �.~�.~�������, �.~�.~����������~//
  \BibEmph{������ � ����}. \BibDash
\newblock 2006. \BibDash
\newblock \CYRT.~83. \BibDash
\newblock {\cyr\CYRS.}~462.

\bibitem{PlakidaOudov1999}
\selectlanguageifdefined{english}
\BibEmph{Plakida,~N.~M.} Electron spectrum and superconductivity in the
  $t\ensuremath{-}j$ model at moderate doping [Text]~/ N.~M.~Plakida,
  V.~S.~Oudovenko~// \BibEmph{Phys. Rev. B}. \BibDash
\newblock 1999. \BibDash
\newblock Vol.~59. \BibDash
\newblock P.~11949.

\bibitem{PlakidaTc2003}
\selectlanguageifdefined{russian}
�������� � ����-�������������� ��������� ����������������� � ��������
  [{\cyr\CYRT\cyre\cyrk\cyrs\cyrt}]~/ �.~�.~�������, �.~�����, �.~����,
  �.~����~// \BibEmph{����}. \BibDash
\newblock 2003. \BibDash
\newblock \CYRT. 124. \BibDash
\newblock {\cyr\CYRS.}~367--37.

\end{thebibliography}
